#include <iostream>
using namespace std;
/* Estructura de nodo */
#include "programa.h"

#ifndef LISTA_H
#define LISTA_H

class Lista{
	private:
		Nodo *raiz = NULL;
		Nodo *ultimo = NULL;

	public:
		/* constructor*/
		Lista();
		
		/* crea un nuevo nodo, recibe una instancia de la clase Persona. */
		void creaNodo (int num);
		void add_num (Nodo *sig, int num);

		/* ordenar elementos lista  */
		void ordenarLista ();
		/* imprime la lista. */
		void verLista ();

};
#endif
