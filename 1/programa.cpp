#include <iostream>
using namespace std;
/* clases */
#include "Lista.h"
/* definición de la estructura Nodo. */
//#include "programa.h"

void evaluarElem(int num, Lista *negativa, Lista *positiva){
	
	if(num < 0){
		negativa->creaNodo(num);
		negativa->ordenarLista();
	} else {
		positiva->creaNodo(num);
		positiva->ordenarLista();
	}
	
}

void menu(Lista *lista, Lista *negativa, Lista *positiva){
	int opcion;
	
	cout << "\n\t\t MENU\n" << endl;
	cout << " [1] Agregar Número" << endl;
	cout << " [2] Ver Lista de elementos ingresados" << endl;
	cout << " [3] Ver Lista Ordenada enteros positivos" << endl;
	cout << " [4] Ver Lista Ordenada enteros negativos" << endl;
	cout << " [5] Salir" << endl;
	
	cout << "\n > Ingrese su opción: ";
	cin >> opcion;

	while(getchar() != '\n');

	switch(opcion){
		case 1:
			int num;

			cout << "\n > Ingrese un número: ";
			cin >> num;

			lista->creaNodo(num);
			/* enviar nuevo elem a su respectiva lista */
			evaluarElem(num, negativa, positiva);
			
			/* return a menu */
			menu(lista, negativa, positiva);
			break;

		case 2:
			cout << "\n\t Lista Elementos Ingresados" << endl;
			lista->verLista();
			menu(lista, negativa, positiva);
			break;

		case 3:
			cout << "\n\t Lista Ordenada Positivos" << endl;
			//positiva->ordenarLista();
			positiva->verLista();
			menu(lista, negativa, positiva);
			break;

		case 4:
			cout << "\n\t Lista Ordenada Negativos" << endl;
			//negativa->ordenarLista();
			negativa->verLista();
			menu(lista, negativa, positiva);
			break;

		case 5:
			exit(1);
			break;

		default:
			cout << "Opcion no valida" << endl;
			menu(lista, negativa, positiva);
			break;
	}
}

/* función principal. */
int main () {
	/* crea listas */
	Lista *lista = new Lista();
	Lista *negativa = new Lista();
	Lista *positiva = new Lista();

	/* mostrar menu */
	menu(lista, negativa, positiva);

	return 0;
}
