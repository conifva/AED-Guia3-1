# AED-Guia3
Actividades correspondientes a la guia 3-1 de Algortimos y Estructura de Datos.

# Guia3-UI
Tema: Listas enlazadas simples (parte 2)

>Programa que cree una lista desordenada de números y luego la divida en dos listas ordenadas ascendentemente (una para los positivos y otra para negativos)

>	- Crear 3 listas (y nodos); una para todos los elementos ingresados, una para los positivos y otra para los megativos
>	- Agregar elementos a lista respectiva y a la lista combinada
>	- Ordenar elementos en listas positiva y en la negativa
>	- Imprimir listas

# Obtención del Programa
Clonar repositorio, ingresar a la carpeta clonada y a la carpeta '1'. Ejecutar por terminal:
```
g++ programa.cpp Lista.cpp -o programa
make
./programa
```

# Acerca de

El programa presenta inicialmente un menú con 5 opciones. 

La primera consiste en agregar elementos (numeros enteros) a una de las tres listas inicializadas al momento de ejecutar el programa, este valor es evaluado y almacenado posteriormente en una de las dos listas restantes. Las opciones 2, 3 y 4 permiten la visualizacion de los elemetos contenidos en cada lista como señalan las alternativas respectivamente, donde solo en las opciones 3 y 4 estarán los valores ordenados de forma ascendente. Finalmente la quinta opción permite al usuario cerrar el programa.

# Requisitos
- Sistema operativo Linux
- Herramienta de gestion de dependencias make (para Makefile)
- Compilador GNU C++ (g++)

# Construccion
Construido y probado con:
- Ubuntu 18.04.03 LTS
- gcc y g++ version 7.4.0
- GNU Make 4.1
- Editor utilizado: Sublime Text

# Autor
Constanza Valenzuela
