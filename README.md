# AED-Guia3-1
# Guia3-1-UI
Actividades tercera guia (parte 2) de AED 2019 sobre listas enlazadas simples

# Obtención del Programa
Clonar repositorio, ingresar a la carpeta del ejercicio correspondiente (por ejemplo, "ej1") y ejecutar por terminal:
```
g++ programa.cpp Clase_correspondiente.cpp -o programa
make
./programa
```

# Actividades
1. Ejercicio 1

	Escriba un programa que cree una lista desordenada de números y luego la divida en dos listas independientes ordenadas ascendentemente, una formada por los números positivos y otra por los números negativos.

2. Ejercicio 2

	Escriba un programa que cree una lista ordenada con nombres (string). Por cada ingreso, muestre el contenido de la lista.

3. Ejercicio 3

	Escriba un programa que permita mantener una lista de postres ordenada alfabéticamente. Cada postre, debe mantener una lista de sus ingredientes (no necesariamente ordenada). Las acciones mı́nimas que se requieren son:

	* Mostrar el listado de postres.

	* Agregar postre.

	* Eliminar postre.

	* Seleccionar postre para:

		- Mostrar sus ingredientes.

		- Agregar ingrediente.

		- Eliminar ingrediente.


# Requisitos
- Sistema operativo Linux
- Herramienta de gestion de dependencias make (para Makefile)
- Compilador GNU C++ (g++)

# Construccion
Construido y probado con:
- Ubuntu 18.04.03 LTS
- gcc y g++ version 7.4.0
- GNU Make 4.1
- Editor utilizado: Sublime Text

# Autor
Constanza Valenzuela
