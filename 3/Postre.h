#include <iostream>
using namespace std;

#ifndef POSTRE_H
#define POSTRE_H

class Postre {
	private:
		string nombre;
		//ListaIngredientes *listaIngredientes = NULL;

	public:
		/* constructor */
		Postre(string nombre);
		
		/* métodos get */
		string get_nombre();
		//ListaIngredientes *get_listIngredientes();
};
#endif
