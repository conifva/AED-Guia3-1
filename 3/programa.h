#include "Postre.h"

/* define la estructura del nodo. */
typedef struct _Nodo {
	Postre *postre;
	struct _Nodo *sig;
} Nodo;
