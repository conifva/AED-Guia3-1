# AED-Guia3
Actividades correspondientes a la guia 3-1 de Algortimos y Estructura de Datos.

# Guia3-UI
Tema: Listas enlazadas simples (parte 2)

>Programa que permita mantener una lista de postres y sus ingredientes, con las opciones de eliminar e ingresar estos datos. 

# Obtención del Programa
Clonar repositorio, ingresar a la carpeta clonada y a la carpeta '3'. Ejecutar por terminal:
```
g++ programa.cpp Lista.cpp -o programa
make
./programa
```

# Acerca de

El programa presenta inicialmente un menú con 5 opciones. 

La primera consiste en agregar el titulo a nuestra receta. La opcion 2 es la eliminacion de alguna receta de la lista anteriormente mencionada. La tercera opción consiste en la busqueda y seleccion de una receta, esta opcion despliega un menu secundario que se explicara detalladamente mas adelante. La opcion 4 permite la visualizacion de la lista de recetas que disponibles.Finalmente la quinta opción le permite al usuario cerrar el programa.

El menu secundario tiene acceso a la informacion exclusiva de cada postre y cuenta basicamente con las mismas opciones anteriores para las recetas pero en este caso se hace referencia a los ingredientes. Cuenta con 4 opciones; agregar un ungrediente, eliminar un ingrediente, visualiza la lista de ingredientes del Postre y volver al menu principal.

# Requisitos
- Sistema operativo Linux
- Herramienta de gestion de dependencias make (para Makefile)
- Compilador GNU C++ (g++)

# Construccion
Construido y probado con:
- Ubuntu 18.04.03 LTS
- gcc y g++ version 7.4.0
- GNU Make 4.1
- Editor utilizado: Sublime Text

# Autor
Constanza Valenzuela
