#include <iostream>
using namespace std;

#include "programa.h"
#include "Lista.h"

Lista::Lista() {}

void Lista::crearPostre (Postre *postre) {
	Nodo *tmp;

	/* crea un nodo */
	tmp = new Nodo;
	/* asigna la instancia de Postre. */
	tmp->postre = postre;
	/* apunta a NULL por defecto. */
	tmp->sig = NULL;

	/* si el es primer nodo de la lista, lo deja como raíz y como último nodo. */
	if (this->raiz == NULL) { 
		this->raiz = tmp;
		this->ultimo = this->raiz;
	/* de lo contrario, apunta el actual último nodo al nuevo y deja el nuevo como el último de la lista. */
	} else {
		this->ultimo->sig = tmp;
		this->ultimo = tmp;
	}
	num_nodos++; //cant postres
}

void Lista::ordenarLista(){
	string puntero; //auxiliar
	Nodo *tmp = this->raiz;

	/* recorre lista mientras q el sucesor del primer name sea nulo */
	while (tmp != NULL){
		Nodo *tmp2 = tmp->sig;
		/* recorre lista mientras el nodo sucesor (tmp2) no sea nulo */
		while (tmp2 != NULL) {
			/* si el primer nodo es mayor que el sucesor */
			if (tmp->postre->get_nombre() > tmp2->postre->get_nombre()) {
				/* auxiliar (puntero) guarda el nodo sucesor */
				puntero = tmp2->postre->get_nombre();
				/* sucesor para a ser el nodo anterior */
				tmp2->postre->get_nombre() = tmp->postre->get_nombre();
				/* primer nodo toma valor del auxiliar */
				tmp->postre->get_nombre() = puntero;
			}
			/* avanza de nodo para nueva comparacion */
			tmp2 = tmp2->sig;
		}
		/* avanza de nodo para nueva comparacion */
		tmp = tmp->sig;
	}	
}

void Lista::delete_posicion(int pos){
	Nodo *tmp = this->raiz;
	Nodo *tmp2 = tmp2->sig;

	if (pos < 1 || pos > num_nodos) {
		cout << " Fuera de rango" << endl;
	} else if (pos == 1) {
		raiz = tmp->sig;
	} else {
		for (int i = 2; i <= pos; ++i) {
			if (i == 0) {
				Nodo *aux_nodo = tmp2;
				tmp->sig = tmp2->sig;
				delete aux_nodo;
				num_nodos--;
			}
			tmp = tmp->sig;
			tmp2 = tmp2->sig;
		}
	}
}

void Lista::verLista () { //recorrer lista
	/* utiliza variable tmporal para recorrer la lista. */
	Nodo *tmp = this->raiz;
	int contador = 0;

	if (!tmp) {
		cout << "\n La Lista está vacía " << endl;
	} else {
		/* la recorre mientras sea distinto de NULL (no hay más nodos). */
		while (tmp != NULL) {
			contador ++;
			cout << "\t [" << contador << "] " << tmp->postre->get_nombre() << endl;
			if (!tmp->sig){
				cout << "\t NULL";
				tmp = tmp->sig;
			} else {
				tmp = tmp->sig;
			}
		}
	}
}
