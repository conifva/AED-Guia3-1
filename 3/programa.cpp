#include <iostream>
using namespace std;

/* definición de la estructura Nodo. */
#include "programa.h"

/* clases */
#include "Postre.h"
#include "Lista.h"


class programa {
	private:
		Lista *lista = NULL;

	public:
		/* constructor */
		programa() {
			this->lista = new Lista();
		}
		
		Lista *get_lista() {
			return this->lista;
		}
};

/* menu sobre los ingredientes de un postre */
void menu2(Lista *lista){
	int opcion2;
	string ingrediente = "m";
	
	cout << "\n\t\tMENU SECUNDARIO\n" << endl;
	cout << " [1] Agregar Ingrediente" << endl;
	cout << " [2] Eliminar Ingrediente" << endl;
	cout << " [3] Ver Lista Ordenada de Postres" << endl;
	cout << " [4] Salir" << endl;
	
	cout << "\n > Ingrese su opción: ";
	cin >> opcion2;

	while(getchar() != '\n');

	switch(opcion2){
		case 1: //add ingrediente
			cout << "\n > Ingrese un ingrediente: ";
			getline(cin, ingrediente);

			/*lista->creaNodo(name);
			lista->ordenarLista();

			/* return a menu */
			//menu2(lista);
			break;

		case 2: //borrar ingrediente
			cout << "\n\t Borrar Ingrediente" << endl;
			//TO DO
			//deleteIngrediente(lista);

			//menu2(lista);
			break;

		case 3: //lista ingredientes
			cout << "\n\t **** Listado de Ingrediente ***" << endl;
			lista->ordenarLista();
			lista->verLista();
			//menu2(lista);
			break;

		case 4:
			//menu2(lista);
			break;

		default:
			cout << " Opcion no valida" << endl;
			//menu2(lista);
			break;
	}
}

/* menu postres */
void menu(Lista *lista){
	
	int opcion;
	string name = "n";
	int pos;

	
	cout << "\n\t\tMENU\n" << endl;
	cout << " [1] Agregar Postre" << endl;
	cout << " [2] Eliminar Postre" << endl;
	cout << " [3] Seleccionar Postre" << endl;
	cout << " [4] Ver Lista Ordenada de Postres" << endl;
	cout << "\n [5] Salir" << endl;
	
	cout << "\n > Ingrese su opción: ";
	cin >> opcion;

	while(getchar() != '\n');

	switch(opcion){
		case 1: //add postre
			cout << "\n > Ingrese un nombre: ";
			getline(cin, name);

			//Postre *postre = new Postre(name);
			lista->crearPostre(new Postre(name));
			lista->ordenarLista();

			/* return a menu */
			menu(lista);
			break;

		case 2: //borrar postre
			cout << "\n\t Borrar Postre" << endl;
			//TO DO
			//selecPostre(lista);

			lista->verLista();
			cout << "\n\t > Ingrese su opción: ";
			cin >> pos;
			lista->delete_posicion(pos);
			lista->ordenarLista();
			cout << "\n\t **** NUeva Listado Postres" << endl;
			lista->verLista();

			menu(lista);
			break;

		case 3: //selec postre
			cout << "\n\t Seleccionar Postre" << endl;
			lista->verLista();
			cout << "\n\t > Ingrese su opción: ";
			cin >> pos;

			//menu2();
			
			menu(lista);
			break;

		case 4: //lista postres
			cout << "\n\t **** Listado de Postres ***" << endl;
			lista->ordenarLista();
			lista->verLista();
			menu(lista);
			break;

		case 5:
			exit(1);
			break;

		default:
			cout << " Opcion no valida" << endl;
			menu(lista);
			break;
	}
}

/* función principal. */
int main (void) {
	/* iniciar preograma y creacion de la lista */ 
	programa p = programa();
	Lista *lista = p.get_lista();

	/* mostrar menu */
	menu(lista);
	return 0;
}
