#include <iostream>
using namespace std;

#ifndef LISTA_H
#define LISTA_H

class Lista {
	private:
		Nodo *raiz = NULL;
		Nodo *ultimo = NULL;
		int num_nodos = 0;

	public:
		/* constructor*/
		Lista();
		
		/* crea un nuevo nodo, recibe una instancia de la clase Postre. */
		void crearPostre (Postre *postre);
		/* ordenar elementos lista  */
		void ordenarLista ();
		/* imprime la lista. */
		void verLista ();

		void delete_posicion(int pos);
};
#endif
