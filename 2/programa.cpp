#include <iostream>
#include <string>
using namespace std;
/* clases */
#include "Lista.h"
/* definición de la estructura Nodo. */
//#include "programa.h"

class programa {
	private:
		Lista *lista = NULL;

	public:
		/* constructor */
		programa() {
			/* crea lista */ 
			this->lista = new Lista();
		}
		
		Lista *get_lista() {
			return this->lista;
		}
};

void menu(Lista *lista){
	
	int opcion;
	string name = "n";
	
	cout << "\n\t\tMENU\n" << endl;
	cout << " [1] Agregar Nombre" << endl;
	cout << " [2] Ver Lista Ordenada" << endl;
	cout << " [3] Salir" << endl;
	
	cout << "\n > Ingrese su opción: ";
	cin >> opcion;

	while(getchar() != '\n');

	switch(opcion){
		case 1:
			cout << "\n > Ingrese un nombre: ";
			getline(cin, name);

			lista->creaNodo(name);
			/* return a menu */
			menu(lista);
			break;

		case 2:
			cout << "\n\t Lista Ordenada" << endl;
			//TO DO
			lista->ordenarLista();
			lista->verLista();
			menu(lista);
			break;

		case 3:
			exit(1);
			break;

		default:
			cout << " Opcion no valida" << endl;
			menu(lista);
			break;
	}



}

/* función principal. */
int main () {
	/* iniciar preograma y creacion de la lista */ 
	programa p = programa();
	Lista *lista = p.get_lista();

	/* mostrar menu */
	menu(lista);
	return 0;

}
