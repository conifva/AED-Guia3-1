# AED-Guia3
Actividades correspondientes a la guia 3-1 de Algortimos y Estructura de Datos.

# Guia3-UI
Tema: Listas enlazadas simples (parte 2)

>Programa que cree una lista ordenada con nombres (tipo string).

>	- Crear lista (y nodos)
>	- Agregar string (nodos)
>	- Ordenar elementos 
>	- Imprimir lista

# Obtención del Programa
Clonar repositorio, ingresar a la carpeta clonada y a la carpeta '2'. Ejecutar por terminal:
```
g++ programa.cpp Lista.cpp -o programa
make
./programa
```

# Acerca de

El programa presenta inicialmente un menú con 3 opciones. 

La primera consiste en agregar nombres (tipo string) a la lista inicializada al momento de ejecutar el programa. La segunda opción permite la visualizacion de los elemetos ingresados por el usuario de forma alfabetica. Finalmente la tercera opción permite al usuario cerrar el programa.

# Requisitos
- Sistema operativo Linux
- Herramienta de gestion de dependencias make (para Makefile)
- Compilador GNU C++ (g++)

# Construccion
Construido y probado con:
- Ubuntu 18.04.03 LTS
- gcc y g++ version 7.4.0
- GNU Make 4.1
- Editor utilizado: Sublime Text

# Autor
Constanza Valenzuela
