#include <iostream>
#include <string>
using namespace std;

/* Definición de la clase */
#include "Lista.h"


Lista::Lista() {}

void Lista::creaNodo(string name){
	Nodo *tmp;

	/* crea un nodo */
	tmp = new Nodo;
	/* asigna al nodo sig */
	tmp->name = name;
	/* apunta a NULL por defecto */
	tmp->sig = NULL;

	/* si el es primer nodo de la lista, lo deja como raíz y como último nodo. */
	if (this->raiz == NULL) { //si la lista esta vacia
		this->raiz = tmp;
		this->ultimo = this->raiz;
	/* de lo contrario, apunta el actual último nodo al nuevo y deja el nuevo como el último de la lista. */
	} else { //lista tiene elementos
		this->ultimo->sig = tmp;
		this->ultimo = tmp;
	}
}
 
void Lista::add_name(Nodo *sig, string name){
	Nodo *tmp = this->raiz;

	tmp->name = name;
	
}


void Lista::ordenarLista(){
	string puntero; //auxiliar
	Nodo *tmp = this->raiz;

	/* recorre lista mientras q el sucesor del primer name sea nulo */
	while (tmp != NULL){
		Nodo *tmp2 = tmp->sig;
		/* recorre lista mientras el nodo sucesor (tmp2) no sea nulo */
		while (tmp2 != NULL) {
			/* si el primer nodo es mayor que el sucesor */
			if (tmp->name > tmp2->name) {
				/* auxiliar (puntero) guarda el nodo sucesor */
				puntero = tmp2->name;
				/* sucesor para a ser el nodo anterior */
				tmp2->name = tmp->name;
				/* primer nodo toma valor del auxiliar */
				tmp->name = puntero;
			}
			/* avanza de nodo para nueva comparacion */
			tmp2 = tmp2->sig;
		}
		/* avanza de nodo para nueva comparacion */
		tmp = tmp->sig;
	}
	
}

void Lista::verLista () {   //recorrer lista
	/* utiliza variable tmporal para recorrer la lista. */
	Nodo *tmp = this->raiz;
	int contador = 0;

	if (!tmp) {
		cout << "\n La Lista está vacía " << endl;
	} else {
		/* la recorre mientras sea distinto de NULL (no hay más nodos). */
		while (tmp != NULL) {
			contador ++;
			cout << "[" << contador << "] " << tmp->name << endl;
			if (!tmp->sig){
				cout << "NULL";
				tmp = tmp->sig;
				//contador ++;
			} else {
				tmp = tmp->sig;
				//contador ++;
			}
		}
	}
}
